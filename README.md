# Guia2- AED " PILAS CON ARREGLOS ---- PUERTO SECO ----" 

# Comenzando
En el desarrollo de la segunda guia se debe realizar con el lenguaje C++, implementando clases para la construccion de este en conjunto con su respectivo archivo de cabecera y Makefile para la compilacion y ejecucion del programa. En la actividad se debe modelar el comportamiento de un puerto seco, se simular que se guarda mercaderia en los contenedores para esto hay un limite de contenedores que se pueden superponer a otros y el area debe estar delimitada tambien, estos valores tanto n como m (numero de contenedores y area) deben leerse por terminal y ademas cada uno tiene un cierto nombre de su compañia y un determinado numero.
Se debe permitir de modo general que salgan o entren un numero de contenedores, cabe destacar que cuando se quiere retirar un contenedor deben salir los otros anteriores a el. La distribucion de los contenedores depende del desarrollador debido a que pueden ser ingresados de manera manual o automatica. Se debe mostrar en pantalla como es el funcionamiento y los movimientos que realizan los contenedores.

---- COMO SE DESARROLLO ----
El programa se ha desarrollado principalmente con el Archivo Pila.h, el cual sirve de cabecera y codigo fuente de la clase donde contiene la clase Pila que cual tendra dentro de ella el objeto contenedor se decidio hacer de esta manera y no por medio de otro archivo porque resulta mas facil de desarrollar la idea que se esta solicitando, luego se declaran los atributos publicos y privados estos son fundamentales para el desarrollo del trabajo, principalmente se define ademas el puntero de la clase contador, en los atributos de caracter publico se exponen las funciones con sus tipos y ademas los atributos que permite que el programa funcione y son ejecutadas en el archivo pila.cpp que las crea y se encarga de su funcionamiento.
Luego se crea el archivo Pila.cpp este archivo llama al archivo de cabecera pila.h tambien se defineun contenedor que tendra el atributo NULL lo cual significa que esta vacio. Luego se crean las funciones que llevaran a cabo el programa y permitira su funcionamiento, las funciones son:
- contenedor_insertar: esta funcion es la encargada de crear la funcion en el objeto Pila que permita que se inserte un contenedor para esto se llama al objeto y un puntero de este, se establecen las condiciones basicas para poder llenar el arreglo con los tenedores y si ya esta ocupada seguir apilando hasta que llegue a un tope.
- contenedor_extraer: esta funcion es la encargada de extraer el contenedor dependiendo de los valores y si no hay un vacio en la matriz se va eliminado el puntero del objeto, se procedio a ocupar Delete para borrar el contenedor, se ocupo de este modo debido a que este operador elimina los objetos que se crearon en especial dentro de las matrices y luego retornar el valor de los contenedores descontando el eliminado anteriormente. 
- contenedor_cantidad: se crea la funcion que permitira conocer el numero de los contenedores que hay en el puerto por medio de un puntero, se recorrera el arreglo y cada vez que no hay algo vacio se aumentara la cantidad y se recorrera el arreglo.
- contenedor mostrar: esta funcion es la que permite mostrar la forma de los contenedores dependiendo de la forma del arreglo imprimiendo ademas el nombre de la empresa y el numero del contenedor. 
- contenedor_ultimo: esta funcion es la que se encarga de ver si el ultimo contenederor es que esta en el arreglo, segun si el ultimo dato agregado es vacio. 
- contenedor_posicion: esta funcion permite obtener y recorrer la posicion del contenedor con el nombre de la empresa y el numero. Dentro de esta funcion se asignaton los valores string de este modo pueden ser mostrados al usuario de mejor manera se ocupo ss que la funcion principal que hace es convertir una variable  ademas cuando se encuentra y se recorre la matriz esto es luego impreso y si no se encuentra imprimira un espacio vacio 
- contenedor_buscar: esta funcion permite buscar un contenedor especifico indicando en el area donde se encuentra se puede buscar el contenedor segun el nombre de la empresa y ademas el numero de esta, se va reccoriendo con estas dos condiciones y si los encuentra sera un valor True, de modo contrario un False indicandoclaramente que no se encontro el objeto.
vacia: es la funcion que se encarga de ver si el array esta vacio para añadir cosas o para ver si se elimino un dato y quedo con 0 datos.

Luego se llama al arhcivo ProgramaPila.cpp este es el programa principal el cual permite la interaccion con el usuario, tiene dos funciones principales la primera muestra el menu de opciones al usuario y la segunda es el main en donde se llevan a cabo las condiciones de las opciones que ingresen las opciones y se llaman a las funcion, en el menu permite:
- Ingresar la cantidad de datos a apilar, apilar los contenedores ingresando el nombre, numero y area donde quiere que esten, tambien en el tercer caso ver los datos que estan dispobibles para mostrar el array, buscar un contenedor segun su nombre y numero, retirar un contenedor bajo estas mismas condicones mostrando finalmente los datos que quedan en el arreglo, la penultima funcion permite cerrar el programa. 
El menu se logra ejecutar con exito, los archivos compilan y funcionan. 
Finalmente cabe destacar que se ha añadido una ultima funcion esta se ha evaluado de esta manera debido a que permite optimizar y no tener tantas lineas de codigo, de esta manera cuando se necesite imprimir el arreglo solo se llama a la funcion, esta funcion ira recorriendo las filas y columnas del arreglo tambien ira de la mano con la funcion que permite conocer la posicion del contenedor debido a que de esta forma se puede imprimir lo que se desea y de manera ordenada. Se implementan guiones medios para establecer separaciones entre las filas y la columnas con la barra vertical, para imprimir tambien son necesarios los saltos de linea debido a que de esta manera permite que la impresion sea ordenada. El valor de k depende del largo del resultado por medio de length se realiza debido a que se puede encontrar el largo de un string. 

A modo de conclusion se ha buscado una posible solucion a la problematica, tambien todos los archivos que componene el desarrollo de esta guia compilan de buena manera con el editor de texto que se esta ejecutando el trabajo.

------EXPLICACION------
Se decidio implementar con otra forma aparte de push y pop debido a que se puede desarrollar de mejor manera sin problemas de errores debido a que se intento con push y pop pero se presentaban muchos errores es por esto que se busco otra manera de desarrollar el codigo, aparte se implementa el uso de punteros. Ademas con este nuevo metodo fue mas facil la creacion del codigo debido a que con las condiciones y los punteros se podian borrar los contenedores y trabajar con condiciones que permitan su manipulacion.
Se reduce ademas la creacion de dos archivos por una clase que contenga a la otra debido a que de esa manera se trabaja con ambas simultaneamente sin mayores problematicas. En el ProgramaPila.cpp se queria hacer una funcion para resumir lo que suceda en el caso 5 cuando se quiere retirar un contenedor pero no se logro encontrar una manera eficiente de hacerlo debido a que si se hacia en una funcion por separado no se contendrian los datos y se declararian con 0 impidiendo el buen funcionamiento del programa y causando una serie de programas.
 El valor del arreglo en este caso el atributo del numero de las columnas que hace referencia al tamano sera determinado con 5 debido a que se considera un numero adecuado de columnas para crear los contenedores, este valor estara determinado. Aunque el valor del numero de los contenedores a apilar se le pedira ingresar al usuario, si este no ingresa nada se determinara automaticamente con 5 filas y 5 columnas.

# Prerequisitos
- Sistema operativo Linux versión igual o superior a 18.04
- Editor de texto (vim o geany)


# Instalacion
Para poder ejecutar el programa se debe conocer que versión de Ubuntu presenta la computadora. Ejecutar este comando:
lsb_release -a (versión de Ubuntu)
En donde:
Se puede corroborar qué versión se tiene instalada actualmente, debido a que se pueden presentar problemas si esta no es compatible con la aplicación que se está desarrollando.
Para descargar un editor de texto como vim se puede descargar de la siguiente manera:
sudo apt install vim
En donde: Por medio de este editor de texto se puede construir el codigo.
En el caso del desarrollo del trabajo se implemento el editor de texto Geany, descargado de Ubuntu Software.

## Ejecutando Pruebas
Cada archivo se puede ejecutar para ver si tiene errores por medio del comando en terminal g++ Nombre del archivo.cpp -o Nombre Archivo. o por medio de un archivo Makefile donde se reunen las condiciones para hacer funcionar el programa

# Construido con
- Ubuntu: Sistema operativo.
- C++: Lenguaje de programación.
- Geany: Editor de código.

# Versiones
Versiones de herramientas:
Ubuntu 20.04 LTS
Geany 1.36-1build1
Versiones del desarrollo del codigo: https://gitlab.com/frana_cr/guia2-aed

# Autores
Francisca Castillo - Desarrollo del código y proyecto, narración README.

# Expresiones de gratitud
- A los ejemplos en la plataforma de Educandus de Alejandro Valdes: https://lms.educandus.cl/mod/lesson/view.php?id=730534&pageid=13730
- A las Lecturas de internet y videos que me ayudaron a plantear una solucion posible:  https://es.wikibooks.org/wiki/Programación_en_C%2B%2B/Estructuras_II y http://informatica.utem.cl/~mcast/ESDATOS/TADS/Ttema2_0506.pdf   
- http://www.cplusplus.com/reference/sstream/stringstream/stringstream/ (para buscar como convertir valores de un tipo)
- para ver como calcular el largo de un string https://www.tutorialspoint.com/5-different-methods-to-find-length-of-a-string-in-cplusplus y https://www.tutorialspoint.com/5-different-methods-to-find-length-of-a-string-in-cplusplus
- https://www.zator.com/Cpp/E4_9_21.htm (para implementar delete)
- https://www.programarya.com/Cursos/C++/Estructuras-de-Datos/Punteros (para aprender a usar punteros)
- Para aprender a recorrer el array https://parzibyte.me/blog/2019/06/19/recorrer-vectores-cpp/ y https://www.aulafacil.com/cursos/programacion/lenguaje-de-programacion-c/recorrer-un-array-l17036
Al ayudante del modulo se otorgan las gracias por solucionar dudas y ayudar a comprender errores.
