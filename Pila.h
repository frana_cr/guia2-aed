#include <iostream>
using namespace std;

/*Como la guia indica que se apilaran contenedores, se definira la Clase Contenedor
	como los elementos de la PILA
*/
#ifndef PILA_H
#define PILA_H

// Se define la primera clase del objeto Pila
class Pila {
	// DENTRO DE LA CLASE PILA LOS ATRIBUTOS PRIVADOS SON PARTE DE LA CLASE CONTENEDOR
private:
// Se define la clase contenedor la cual contiene los atributos publicos y privados para hacer funcionar el programa
    class Contenedor {
    public:
    // Los atributos publicos son el numero y nombre del contenedor
        int numero;				
        string empresa;	
        // Se define el atributo posicion 
        int posicion;
        // Puntero al siguiente contenedor		
        Contenedor *sig;	
    };
    // Puntero de la clase contenedor
    Contenedor *ultimo;
// En la clase Pila los atributos publicos son
public:
	// Se define el constructor 
    Pila();
    // Se crea la funcion que apila e inserta al contenedor
    void contenedor_insertar(int numero, string empresa);	
    // Se crea la funcion que desapila el contenedor	
    int contenedor_extraer();	
    // Se crea la funcion que muestra la cantidad de los contenedores								
    int contenedor_cantidad();	
    // Se crea la funcion que permite mostrar todos los contenedores								
    void contenedor_mostrar();
    // Se crea la funcion que muestra el ultimo contenedor					
    void contenedor_ultimo(int &numero, string &empresa);
    // Se crea la funcion que devuelve el contenedor en una posicio
    string contenedor_posicion(int posicion);
	// Se crea la funcion de tipo booleano que busca el contenedor
    bool contenedor_buscar(int numero, string empresa);	
    // Se crea la funcion que devuelve si la pila esta o no vacia		
    bool vacia();								
};
#endif