prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = ProgramaPila.cpp Pila.cpp
OBJ = ProgramaPila.o Pila.o
APP = ProgramaPila

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
