/*
 * g++ Pila.cpp -o Pila
 *
 * o ejecutar:
 *
 * make
 */

#include <iostream>
#include<sstream>
using namespace std;
// Se llama al archivo pila que es el cabecera
#include "Pila.h"
// Se llama al constructor
Pila::Pila(){
	// Se define al ultimo contenedor como Null(vacio)
	ultimo = NULL;
}
// Se crea la funcion Pila que insertara el contenedor
void Pila::contenedor_insertar(int numero, string empresa){
	// Creamos el puntero
    Contenedor *nuevo;
    // Creamos el objeto del tipo Contenedor
    nuevo = new Contenedor();
    // Asignamos el numero de Contenedor
    nuevo->numero = numero;
    // Asignamos el nombre de la empresa al Contenedor
    nuevo->empresa = empresa;
    // Posicion en la pila
    nuevo->posicion = this->contenedor_cantidad()+1;

    // Se establece la condicion que si la pila esta vacia, se agregara a la ultima posicion el primer elemento
    if (ultimo == NULL){
        ultimo = nuevo;
        nuevo->sig = NULL;
    }
    else{
		// Si la Pila no esta vacia apila el nuevo Contenedor sobre ultimo apilado
        nuevo->sig = ultimo;
        ultimo = nuevo;
    }
}

// Se crea la funcion que extrae al contenedor
int Pila::contenedor_extraer(){
	// Si el valor de ultimo es diferente a vacio, significa que la Pila tiene elementos
    if (ultimo != NULL){
		// Se almacena el numero del contenedor
        int numero = ultimo->numero;
        // Se crea el puntero borrar
		Contenedor *borrar = ultimo;
		// Se mueve el puntero al "nuevo" ultimo
        ultimo = ultimo->sig;
        // Se Elimina el contenedor, se ocupa delete para borrar un objeto creador en la matriz.
        delete borrar;
        //Se retorna el numero del contenedor eliminado
        return numero;
    }else{
		//En el caso de pila Vacia se retorna -1 al programa principal
        return -1;
    }
}

// Se crea la funcion que permite conocer la cantidad de los contenedores de la pila
int Pila::contenedor_cantidad(){
	// Se crea un puntero que permita recorrer y revisar los elementos del array
    Contenedor *recorrer = ultimo;
    // La cantidad tendra el valor de 0
    int cantidad = 0;
    // Cuando el valor de recorrer sea distinto de vacio
    while (recorrer != NULL){
		// El valor de cantidad aumentara
        cantidad++;
        recorrer = recorrer->sig;
    }
    return cantidad;
}

// Se crea la funcion para mostrar el contenedor
void Pila::contenedor_mostrar(){
	// Se crea el puntero que recorre y revise los elementos
    Contenedor *recorrer = ultimo;
    cout << "-----------------" << endl;
    // Cuando el valor recorrer sea diferente a null
    while (recorrer != NULL){
		// Se imprime el numero y nombre de la empresa del contenedor
        cout << "| " << recorrer->numero << " - " << recorrer->empresa << endl;
        cout << "-----------------" << endl;
        recorrer = recorrer->sig;
    }
}

// Se crea la funcion que ve al ultimo contenedor
void Pila::contenedor_ultimo(int &numero, string &empresa){
	// Se ve si el valor ultimo es distinto de vacio
    if (ultimo != NULL){
		// Se almacenena el numero del contenedor
        numero = ultimo->numero;
        // Se almacena la empresa del contenedor
        empresa= ultimo->empresa;
    }
}

// Se crea la funcion para saber la posicion del contenedor, con el atributo posicion
string Pila::contenedor_posicion(int posicion){
	// Se define la variable con su tipo
	int encontrado;
	string res1;
	string res2;
	stringstream ss;

	// Se crea un puntero "recorrer" para revisar los elementos
	Contenedor *recorrer = ultimo;
	// Cuando el valor de recorrer sea distinto de vacio
	while (recorrer != NULL){
		// Se utiliza la posicion buscada para saber donde imprimirlo en pantalla
		if(recorrer-> posicion==posicion){
			res1 = recorrer->empresa;
			//Se pasa una variabla de tipo INT a STRING
			ss << recorrer->numero;
			//Se asigna la variable convertida a res2
			ss >> res2;
			//Se concatena la respuesta antes de retornar
			res1=res1+"-"+res2;
			//Se indica que fue encontrada por eso el valor es 1
			encontrado=1;
		}
		recorrer = recorrer->sig;
	} if(encontrado!=1){
		// Si no se encuentra  un contenedor en la posicion se declara vacio
		res1=" ";
	}
	return res1;
}

// Se crea la funcion que busca el contenedor, con el atributo del numero
bool Pila::contenedor_buscar(int numero, string empresa){

	// Se crea un puntero que recorra y revise los elementos del array
	Contenedor *recorrer = ultimo;
	// Cuando recorrer sea distinto de null (vacio)
	while (recorrer != NULL){
		// Si el recorrer es igual al numero y al nombre de la empresa
		if(recorrer->numero==numero && recorrer->empresa==empresa){
			// Se retorna un valor true
			return true;
		}
		// Recorrer pasara al siguiente
		recorrer = recorrer->sig;
	}
		// Si termina el recorrido del Array sin encontrar el numero que se ingresa se retornara el false
	return false;
}

// Se crea la funcion de tipo booleano en la cual se ve si esta vacia o no el array
bool Pila::vacia(){
	// Si el ultimo contenedor es igual a 0 se retornara un valor true
	if (ultimo == NULL){
        return true;
	}
        // De modo contrario se retorna un false.
    else{
        return false;
	}
}