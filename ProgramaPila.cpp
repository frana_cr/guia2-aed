/*
 * g++ ProgramaPila.cpp Pila.cpp -o Pila
 *
 * o ejecutar:
 *
 * make
 */

#include <iostream>
using namespace std;
// Se llama al archivo Pila.cpp que contiene las funciones
#include "Pila.cpp"

// Se crea la funcion que contiene el menu
int opciones_menu(){
	// se solicita al usuario ingresar su opcion
    int opcion;
    // Se muestra la informacion en pantalla
    cout << " INFORMACION RESPECTIVO PUERTO SECO" << endl;
    // Se muestra el menu de opciones
	cout << "OPCION 1 - INGRESE CANTIDAD DE CONTENEDORES A APILAR" << endl;
	cout << "OPCION 2 - APLILAR LOS CONTENEDOR" << endl;
	cout << "OPCION 3 - REVISAR LOS CONTENEDORES" << endl;
	cout << "OPCION 4 - BUSCAR UN CONTENEDOR" << endl;
	cout << "OPCION 5 - RETIRAR UN CONTENEDOR" << endl;
	cout << "OPCION 0 - SALIR DEL PROGRAMA" << endl;
	cout << "---------------------" << endl;
	// Se imprime para que el usuario vea cuando agregar la opcion
	cout << "Ingrese su opcion:";
	cin >> opcion;
	return opcion;
}

// ESTA FUNCION PERMITE MOSTARA LOS CONTENEDORES
/*se hizo de esta manera para ahorrar codigo y no mostararla en cada funcion que se necesitaba mostrar el array.*/
void mostrar_contenedores(int alto, Pila *pilas){
	string resultado;
	// POR medio de estos for se podra imprimir la forma del arreglo con columnas y filas
	// Se recorre i
	for(int i=alto; i>0; i--){
		// Se recorre j
		for(int j=0; j<5; j++){
			// la variable resultado contendra a la pila en su posicion con columna y fila
			resultado=pilas[j].contenedor_posicion(i);
			// De esta manera con la barra vertical se imprimen los resultados
			cout << "|  " << resultado;
			// Cuando el for tenga un valor que el largo del resultado es igual a k y esta menor a 20, sera vacio
			for(int k=resultado.length(); k<20; k++){
				cout << " ";
			}
		}
		cout << " |" << endl;
		// Cuando el valor de k es igual a 0 y menor a 115 se calculo esta valor porque asi queda exacto y no se sobrepasa la impresion
		for(int k=0; k<115; k++){
			// Se imprime un guion medio
				cout << "-";
			}
			// Se necesitan estas impresiones con saltos de linea para poder diferencias las estructuras
		cout << endl;
	}
	cout << endl << endl;
}
// Se define el menu principal
int main(){
	// Se solicitan los datos con su valor
	/* Se definen por orden n: es numero de contenedores, opcion: opcion ingresada por el usuario area: el valor del area,
	 * area aux y area aux2: mostraran la informacion extra al usuario, apilados: contendra el valor de los datos que se desea apilar,
	 * aux sera la condicion para los ciclos*/
	int n;
	int opcion;
	int area;
	int area_aux;
	int area_aux2;
	int apilados;
	int aux;

	// La pila contendra por defecto 5 areas
	Pila areas[5];
	// La cantidad por apilar sera maximo 5
	n=5;
	// Se define el nombre de la empresa a ingresar
	string empresa;
	// Se define el nombre que se ingresara para buscar
	string empresa_aux;
	// Se define el numero de la empresa a ingresar
	int numero;
	// Se define el nombre que se ingresara para buscar
	int numero_aux;
	// SE GENERA EL CICLO SEGUN LAS OPCIONES
	while (opcion!=0){
		opcion = opciones_menu();
		switch(opcion){
			// Si el usuario elige la PRIMERA opcion sucedera lo siguiente
			case 1:
				cout << "INGRESANDO CANTIDAD A APILAR";
				cout << "---------------" << endl;
				cout << "INGRESE LA CANTIDAD DE CONTENEDORES A APILAR" << endl;
				cin >> n;
				// Se llama a la funcion que muestra los contenedores
				mostrar_contenedores(n, areas);
				break;

			// Si el usuario elige la SEGUNDA opcion
			case 2:
				cout << "APILANDO CONTENEDORES" << endl;
				cout << "--------------" << endl;
				cout << " INGRESE EL NOMBRE DE LA EMPRESA DEL CONTENEDOR: ";
				cin >> empresa;
				cout << "INGRESE EL NUMERO DEL CONTENEDOR: ";
				cin >> numero;
				cout << "INGRESE EL AREA DONDE SE APILARA EL CONTENEDOR: ";
				cin >> area;
				area-=1;
				// Se ve el numero de los apilados
				apilados=areas[area].contenedor_cantidad();
				// Se establece la condicion que si el numero de apilados es menor a n(5)
				if(apilados < n){
					// Se puede apilar el contenedor
					areas[area].contenedor_insertar(numero, empresa);
				}else{
					// De modo contrario no se podra apilar y se muestra que esta lleno
					cout << "NO ES POSIBLE APILAR, el limite de apilamiento es: " << n << endl;
					areas[area].contenedor_mostrar();
				}
				// Se llama a la funcion que muestra los contenedores
				mostrar_contenedores(n, areas);
				break;

			// Si el usuario elige la TERCERA opcion sucedera lo siguiente
			case 3:
				cout << "REVISANDO LOS CONTENEDORES" << endl;
				cout << "_---------------------" << endl;
				cout << "INGRESE EL AREA A REVISAR: ";
				cin >> area;
				// La posicion que ingrese el usuario tiene que tener un valor menos al programa
				area-=1;
				// Se muestran los contenedores en el area
				areas[area].contenedor_mostrar();
				// Se llama a la funcion que muestra los contenedores
				mostrar_contenedores(n, areas);
				break;

			// Si el usuario elige la CUARTA opcion sucedera lo siguiente
			case 4:
				area=0;
				cout << "BUSCANDO CONTENDORES" << endl;
				cout << "_---------------------" << endl;
				cout << "INGRESE EL NOMBRE DE LA EMPRESA: ";
				cin >> empresa;
				cout << "INGRESE EL NUMERO DEL CONTENEDOR: ";
				cin >> numero;
				// Se recorre para ver si el area es vacia
				for(int i=0; i<5; i++){
					if(!areas[i].vacia()){
						// Si el area al buscar tiene el numero y empresa
						if(areas[i].contenedor_buscar(numero, empresa)){
							area = i + 1;
							// Se imprime en que area esta el contenedor
							cout << "EL CONTENEDOR ESTA EN EL AREA " << area << endl;
						}
					}
				}
				// Si el area es igual a 0, significa que el contenedor con ese nombre y numero no es encontrado
				if(area==0){
					cout << "CONTENEDOR NO ENCONTRADO" << empresa << " - " << numero << endl;
				}
				// Se llama a la funcion que muestra los contenedores
				mostrar_contenedores(n, areas);
				break;

			// Si el usuario elige la QUINTA opcion sucedera lo siguiente
			case 5:
				area=-1;
				cout << "RETIRANDO CONTENEDOR" << endl;
				cout << "---------------------" << endl;
				cout << "INGRESE NOMBRE DE LA EMPRESA DEL CONTENEDOR: ";
				cin >> empresa;
				cout << "INGRESE EL NUMERO DEL CONTENEDOR: ";
				cin >> numero;

				//Se busca el Area donde esta el contenedor a retirar por eso se hace un ciclo for
				for(int i=0; i<5; i++){
					if(!areas[i].vacia()){
						// SI el area es encontrado
						if(areas[i].contenedor_buscar(numero, empresa)){
							// La area sera igual a i
							area = i;
						}
					}
				}
				// si el valor del area es igual o mayor a 0
				if(area >= 0){
					//Se inicia variable para mantener el ciclo
					aux = 0;
					// Se sacan elementos del area y reacomodarlos
					while(aux == 0){
						// Se ve cual es el ultimo contenedor para hacer el movimiento
						areas[area].contenedor_ultimo(numero_aux, empresa_aux);
						//Si el Contenedor es el que se esta buscando se retira porque es igual la empresa y el numero
						if(numero==numero_aux && empresa==empresa_aux){
							areas[area].contenedor_extraer();
							// Se imprime que el contenedor ha sido borrado
							cout << " EL CONTENEDOR " << empresa << " - " << numero << " SE RETIRO " << endl;
							aux=1;
							// Se llama a la funcion que muestra los contenedores
							mostrar_contenedores(n, areas);
						}
						//Si el Contenedor no es el que se esta buscando se mueve
						else{
							//Se recorren las otras areas para buscar espacios y acomodar los contenedores
							for(int i=0; i<5; i++){
								// Si el valor del area es distinto donde se saca el contenedor
								if(i!=area){
									// Se crea este if para consultar el espacio del contenedor menor al valro n(5)
									if(areas[i].contenedor_cantidad() < n){
										// Se inserta el contenedor
										areas[i].contenedor_insertar(numero_aux, empresa_aux);
										area_aux=area+1;
										area_aux2=i+1;
										//Se muestra por pantalla el movimiento
										cout << "EL CONTENEDOR " << empresa_aux << " - " << numero_aux;
										cout << " ESTABA EN EL AREA: " << area_aux << "AHORA ESTA EN" << area_aux2 << endl;
										//Elimina el contenedor en el origen
										areas[area].contenedor_extraer();
										// Se llama a la funcion que muestra los contenedores
										mostrar_contenedores(n, areas);
										break;
									}
								}
							}

						}
					}
				}
				//Si no encontro el nodo que se pide retirar
				else{
				//Si no encontro el contenedor
					cout << " NO EXISTE DICHO CONTENEDOR" << empresa << " - " << numero << endl;
				// Se llama a la funcion que muestra los contenedores
					mostrar_contenedores(n, areas);
					cout << endl << endl;
				}
				break;

// CUANDO EL USUARIO INGRESE 0
case 0:
            // El programa se cierra
            cout << "CERRANDO PROGRAMA";
            exit(0);
        }
      }
        system("pause");
      return 0;
}